import sys
import argparse
import requests
import warnings
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from urllib.parse import urlparse
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
Patching requests to work without SSL verification.
ie.) 
url = 'https://www.google.com'
request.get(url, verify=False)
"""
def requestspatch(method, url, **kwargs):
    kwargs['verify'] = False
    return _origcall(method, url, **kwargs)

_origcall = requests.api.request
requests.api.request = requestspatch

class SwaggerDocTestTool:
    """
    URL destination of Swagger docs is passed into the constructor when instance is created.
    """
    def __init__(self, url):
        self.test_results = {}
        self._swagger_docs_url = url

        parsed_uri = urlparse(url)
        self._base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
        self.requests = []

        options = Options()
        #options.add_argument('--headless') # runs headless for increased performance
        options.add_argument("window-size=1200x600")
        self._driver = webdriver.Chrome('./chromedriver', options=options)


    def _display_endpoints(self):
        """
        Prints all endpoints scraped from the documentation page.
        """
        print('====== ENDPOINTS ======\n')
        for request in self.requests:
            print('{}\t{}\n'.format(request[0]), request[1])


    def _test_response(self, request):
        """
        Tests if an endpoint responds with a 200 status code.
        Adds test result to instance test_results dictionary.
        """
        # TODO: Consider making a class for each request
        # stores method, endpoint, isTested, testResults

        endpoint = self._base_url + request[1]
        req_method = getattr(requests, request[0])

        try:
            req = req_method(endpoint)
            if req.status_code in [200, 403, 405]: # 200 (OK), 405 (Method Not Allowed), 403 (Forbidden)
                self.test_results[request[1]] = True
        except:
            self.test_results[request[1]] = False


    def main(self):
        """
        Pulls all endpoints from a SwaggerUI
        TODO: Add the version of swagger that this is compatible with (if swagger changes their UI this will have to change as well)
        """
        try:
            self._driver.get(self._swagger_docs_url)
        except:
            raise Exception('Documentation at {} is not available at this time.'.format(self._base_url))

        # search for all page elements that contain the text "/api"
        endpoints = [e.text for e in self._driver.find_elements_by_xpath("//*[contains(text(), '/api')]")]
        endpoint_methods = [em.text.lower() for em in self._driver.find_elements_by_class_name('opblock-summary-method')]

        self.requests = zip(endpoint_methods, endpoints)
        self._test()

        # TODO: THIS DISPLAY FUNCTIONALITY SHOULD BE INTEGRATED WITH TFS
        self._display_endpoints()


    def _test(self):
        for request in self.requests:
            print("TESTING: {}".format(request))
            self._test_response(request)

    
    def show_test_results(self):
        print('====== TEST RESULTS ======\n')
        print(sdtt.test_results)


def load_args():
    """
    Parsing command line arguments.
    """
    parser = argparse.ArgumentParser(description='Arguments for the swagger documentation CI/CD testbed')
    parser.add_argument('-u', '-url', help='Url of the swagger documentation', type=str)

    return vars(parser.parse_args())


args = load_args()
sdtt = SwaggerDocTestTool(args['u'])
sdtt.main()
sdtt.show_test_results()