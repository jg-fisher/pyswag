# pyswag

Command line tool for automated testing of SwaggerUI endpoint validity.

Swagger: https://swagger.io/

## Prerequisites
- Python 3.7
- Git
- Chromedriver (http://chromedriver.chromium.org/downloads - put this in the root directory of the project folder)


## Usage
```
$ git clone https://gitlab.com/jg-fisher/pyswag/
$ cd pyswag
$ pip3 install pipenv
$ pipenv install
$ pip install requests selenium
$ python main.py -u https://localhost:44310/swagger/index.html
```